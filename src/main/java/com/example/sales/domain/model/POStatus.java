package com.example.sales.domain.model;

public enum POStatus {
    PENDING, OPEN, REJECTED, CLOSED
}
