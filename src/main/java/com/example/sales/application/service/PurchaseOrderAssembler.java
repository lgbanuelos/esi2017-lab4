package com.example.sales.application.service;

import com.example.common.application.dto.BusinessPeriodDTO;
import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.application.service.PlantInventoryEntryAssembler;
import com.example.inventory.domain.model.PlantInventoryEntry;
import com.example.sales.application.dto.PurchaseOrderDTO;
import com.example.sales.domain.model.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

// TODO 2.1: Update the class such as it extends Spring hateoas' ResourceAssemblerSupport
@Service
public class PurchaseOrderAssembler {
    @Autowired
    PlantInventoryEntryAssembler plantInventoryEntryAssembler;
    // TODO 2.2: Add constructor as required by Spring hateoas' ResourceAssemblerSupport

    public PurchaseOrderDTO toResource(PurchaseOrder purchaseOrder) {
        PurchaseOrderDTO dto = new PurchaseOrderDTO(); // TODO 2.3: replace new statement with call to createResourceWithId
        dto.set_id(purchaseOrder.getId());
        dto.setPlant(plantInventoryEntryAssembler.toResource(purchaseOrder.getPlant()));
        dto.setRentalPeriod(BusinessPeriodDTO.of(purchaseOrder.getRentalPeriod().getStartDate(),purchaseOrder.getRentalPeriod().getEndDate()));
        dto.setTotal(purchaseOrder.getTotal());
        dto.setStatus(purchaseOrder.getStatus());
        return dto;
    }

    // TODO 2.4: Remove the method below
    public List<PurchaseOrderDTO> toResources(List<PurchaseOrder> pos) {
        return pos.stream().map(po -> toResource(po)).collect(Collectors.toList());
    }
}
