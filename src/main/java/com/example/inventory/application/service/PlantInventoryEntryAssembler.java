package com.example.inventory.application.service;

import com.example.inventory.application.dto.PlantInventoryEntryDTO;
import com.example.inventory.domain.model.PlantInventoryEntry;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

// TODO 4.1: Update the class such as it extends Spring hateoas' ResourceAssemblerSupport
@Service
public class PlantInventoryEntryAssembler {
    // TODO 4.2: Add constructor as required by Spring hateoas' ResourceAssemblerSupport
    public PlantInventoryEntryDTO toResource(PlantInventoryEntry plantInventoryEntry) {
        PlantInventoryEntryDTO dto = new PlantInventoryEntryDTO(); // TODO 4.3: replace new statement with call to createResourceWithId
        dto.set_id(plantInventoryEntry.getId());
        dto.setName(plantInventoryEntry.getName());
        dto.setDescription(plantInventoryEntry.getDescription());
        dto.setPrice(plantInventoryEntry.getPrice());
        return dto;
    }

    // TODO 4.4: Remove the method below
    public List<PlantInventoryEntryDTO> toResources(List<PlantInventoryEntry> plants) {
        return plants.stream().map(plant -> toResource(plant)).collect(Collectors.toList());
    }
}
