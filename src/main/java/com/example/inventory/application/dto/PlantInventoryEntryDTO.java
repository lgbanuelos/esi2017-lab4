package com.example.inventory.application.dto;

import lombok.Data;

import java.math.BigDecimal;

// TODO 3: Update the class such as it extends Spring hateoas' ResourceSupport
@Data
public class PlantInventoryEntryDTO {
    String _id;
    String name;
    String description;
    BigDecimal price;
}
